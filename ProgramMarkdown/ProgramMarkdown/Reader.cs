﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace ProgramMarkdown
{
    class Reader
    {
        private string _location;

        public Reader(string location)
        {
            _location = location;
        }

        public string ReadFileFromFolder(string file)
        {
            string[] fileContents;
            string fileContentsDump;
            string result = "";
            int count = 0;

            if (File.Exists(_location + file))
            {
                fileContentsDump = File.ReadAllText(_location + file);
                fileContentsDump = Regex.Replace(fileContentsDump, @"[{]+", "\n{\n");
                fileContentsDump = Regex.Replace(fileContentsDump, @"[}]+", "\n}\n");
                fileContents = fileContentsDump.Split('\n');
                //fileContents = File.ReadAllLines(_location + file);
            }
            else
                return "Sorry no file!";

            //cleaning up the string
            //referenced from https://stackoverflow.com/questions/1279859/how-to-replace-multiple-white-spaces-with-one-white-space
            for (int i = 0; i < fileContents.Length; i++)
            {
                //fileContents[i] = Regex.Replace(fileContents[i], @"[{]+", "\n{\n");
                //fileContents[i] = Regex.Replace(fileContents[i], @"[}]+", "\n}\n");
                fileContents[i] = Regex.Replace(fileContents[i], @"[(]+", " ( ");
                fileContents[i] = Regex.Replace(fileContents[i], @"[)]+", " ) ");
                fileContents[i] = Regex.Replace(fileContents[i], @"[ \t]+", " ");
            }

            // finding the namespace
            for (int i = 0; i < fileContents.Length; i++)
            {
                if (fileContents[i].ToLower().Contains("namespace"))
                {
                    result = "#" + fileContents[i].Split(' ')[1] + "\n";
                    break;
                }
            }

            List<string> ClassVariables = new List<string>();
            List<string> Properties = new List<string>();
            List<string> Methods = new List<string>();

            // finding the classes
            for (int i = 0; i < fileContents.Length; i++)
            {
                //Console.WriteLine(i+1);
                if (fileContents[i].Contains("{"))
                    count++;
                if (fileContents[i].Contains("}"))
                {
                    count--;
                    //continue;
                }

                if (count == 1)
                {
                    //this means that we are inside the namespace, searching for the classes
                    //can have multiple classes in the namespace/file

                    if (ClassVariables.Count > 0 || Properties.Count > 0 || Methods.Count > 0)
                    {
                        result = result + "###Class Variables" + "\n";
                        for (int j = 0; j < ClassVariables.Count; j++)
                        {
                            result = result + ClassVariables[j];
                        }

                        result = result + "###Properties" + "\n";
                        for (int j = 0; j < Properties.Count; j++)
                        {
                            result = result + Properties[j];
                        }

                        result = result + "###Methods" + "\n";
                        for (int j = 0; j < Methods.Count; j++)
                        {
                            result = result + Methods[j];
                        }
                        ClassVariables.Clear();
                        Properties.Clear();
                        Methods.Clear();
                    }

                    if (fileContents[i].ToLower().Contains("class"))
                    {
                        result = result + "##Class: " + fileContents[i].Split(' ')[Array.IndexOf(fileContents[i].ToLower().Split(' '), "class") + 1] + "\n";
                        ClassVariables.Clear();
                        Properties.Clear();
                        Methods.Clear();
                    }
                    
                    //put something here for enums
                }
                else if (count == 2)
                {
                    //means that we're in the class and looking for methods
                    if (fileContents[i].Split(' ').Length > 2)
                    {
                        //checking if it's not a curly bracket

                        if (fileContents[i].ToLower().Split(' ')[1] == "readonly" || fileContents[i].ToLower().Split(' ')[1] == "public" || fileContents[i].ToLower().Split(' ')[1] == "private" || fileContents[i].ToLower().Split(' ')[1] == "protected")
                        {
                            if (fileContents[i].Contains('(') || fileContents[i].Contains(')'))
                            {
                                //its a method
                                if ((fileContents[i].ToLower().Split(' ')[2].Contains('(')) || (fileContents[i].ToLower().Split(' ')[3] == "(" ) || (fileContents[i].ToLower().Split(' ')[3].ToCharArray()[0] == '('))
                                {
                                    // its a constructor
                                    //result = result + "\n\tMethod: " + System.Text.RegularExpressions.Regex.Replace(fileContents[i].Split(' ')[2], @"[(]+", "");
                                    Methods.Add("- " + fileContents[i].Split(' ')[2] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **constructor**\n");
                                }

                                else if (fileContents[i].ToLower().Split(' ')[2] == "static")
                                    Methods.Add("- " + fileContents[i].Split(' ')[4] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[3] + "**\n  - static\n");

                                else if (fileContents[i].ToLower().Split(' ')[2] == "override")
                                    Methods.Add("- " + fileContents[i].Split(' ')[4] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[3] + "**\n  - static\n");

                                else
                                    Methods.Add("- " + fileContents[i].Split(' ')[3] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[2] + "**\n");
                            }
                            else
                            {
                                //its either a property or class variable
                                if (fileContents[i].Split(' ')[fileContents[i].Split(' ').Length - 1].Contains(";"))
                                {
                                    //class variables
                                    if (fileContents[i].ToLower().Split(' ')[2] == "static")
                                    {
                                        ClassVariables.Add("- " + Regex.Replace(fileContents[i].Split(' ')[4], @"[;]+", "") + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[3] + "**\n  - static\n");
                                    }
                                    if (fileContents[i].ToLower().Split(' ')[2] == "readonly")
                                    {
                                        ClassVariables.Add("- " + Regex.Replace(fileContents[i].Split(' ')[4], @"[;]+", "") + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[3] + "**\n  - readonly\n");
                                    }
                                    if (fileContents[i].ToLower().Split(' ')[2] == "static" && fileContents[i].ToLower().Split(' ')[3] == "readonly")
                                    {
                                        ClassVariables.Add("- " + Regex.Replace(fileContents[i].Split(' ')[5], @"[;]+", "") + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[4] + "**\n  - static\n  - readonly\n");
                                    }
                                    else
                                        ClassVariables.Add("- " + Regex.Replace(fileContents[i].Split(' ')[3], @"[;]+", "") + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[2] + "**\n");
                                }
                                else
                                {
                                    //property
                                    if (fileContents[i].ToLower().Split(' ')[2] == "override")
                                    {
                                        Properties.Add("- " + fileContents[i].Split(' ')[4] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[3] + "**\n  - override\n");
                                    }
                                    else if (fileContents[i].ToLower().Split(' ')[2] == "static")
                                    {
                                        Properties.Add("- " + fileContents[i].Split(' ')[4] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[3] + "**\n  - static\n");
                                    }
                                    else if (fileContents[i].ToLower().Split(' ')[2] == "override" && fileContents[i].ToLower().Split(' ')[3] == "static")
                                    {
                                        Properties.Add("- " + fileContents[i].Split(' ')[5] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[4] + "**\n  - static\n  - override\n");
                                    }
                                    else
                                        Properties.Add("- " + fileContents[i].Split(' ')[3] + "\n  - *" + fileContents[i].Split(' ')[1] + "*\n  - **" + fileContents[i].Split(' ')[2] + "**\n");
                                }
                            }
                        }
                    }
                    
                    
                }
                else
                    continue;

            }

            return result;
            
        }
    }
}
