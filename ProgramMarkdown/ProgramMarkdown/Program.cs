﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ProgramMarkdown
{
    class Program
    {
        static void Main(string[] args)
        {
            ///*
            Console.WriteLine("Please drag the folder of your code on to the terminal: ");
            Reader r = new Reader(Console.ReadLine());
            Console.WriteLine("Thank you!\nPlease Enter the file name you want to analyse (excluding the extension)");

            string result = r.ReadFileFromFolder("/" + Console.ReadLine() + ".cs");
            //Console.WriteLine(result);
            Console.WriteLine("Thank you!\nPlease drag on your destination for the file to go (a folder): ");
            string loco = Console.ReadLine();
            Console.WriteLine("Thank you!\nPlease enter the name of the new markdown file (without the markdown extension): ");
            string name = Console.ReadLine();
            File.WriteAllText(loco + "/" + name + ".md", result);
            //*/
            /*
            Reader r = new Reader("/UNI/SEM2/OOP/SwinAdventure/SwinAdventure");
            Console.WriteLine(r.ReadFileFromFolder("/Test.cs"));
            */
            Console.WriteLine("Done!\nThanks!\nPress enter to continue to exit the program.");
            Console.Read();
        }
    }
}
